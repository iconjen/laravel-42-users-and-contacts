<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts', function($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('ac_id')->unsigned();
            $table->string('email', 128)->unique();
            $table->string('firstname', 25);
            $table->string('lastname', 25);
            $table->string('phone', 15);
            $table->timestamps();
            
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contacts', function($table) {
            $table->dropForeign('contacts_user_id_foreign');
        });
        
        Schema::drop('contacts');
	}

}
