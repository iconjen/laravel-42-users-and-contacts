<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('custom_fields', function($table) {
            $table->increments('id');
            $table->integer('contact_id')->unsigned();
            $table->string('value', 128);
            $table->timestamps();
            
            $table->foreign('contact_id')
                ->references('id')->on('contacts')
                ->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('custom_fields', function($table) {
            $table->dropForeign('custom_fields_contact_id_foreign');
        });
        
        Schema::drop('custom_fields');
	}

}
