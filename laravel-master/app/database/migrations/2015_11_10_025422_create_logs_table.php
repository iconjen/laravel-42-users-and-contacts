<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('logs', function($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('reference_id');
            $table->text('activity');
            $table->timestamps();
            
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('logs', function($table) {
            $table->dropForeign('logs_user_id_foreign');
        });
        
        Schema::drop('logs');
	}

}
