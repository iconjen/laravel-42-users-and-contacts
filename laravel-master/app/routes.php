<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function() {
    return Redirect::to(url('auth/login'));
});
    
Route::controller('auth', 'AuthController');

Route::post('api/users', ['as' => 'auth.postLogin', 'uses' => 'Api\UserController@store']);

Route::group(['before' => 'auth'], function() {
    Route::get('dashboard', 'UserController@getDashboard');
    Route::controller('users', 'UserController');
    
    Route::group(['prefix' => 'api'], function() {
        Route::resource('contacts', 'Api\ContactController');
    });
    
    Route::resource('users', 'Api\UserController', ['except' => ['post']]);
    Route::resource('activecampaign', 'ActiveCampaignController');
});