<?php

class Contact extends Eloquent {
    
    protected $fillable = [
        'user_id',
        'email',
        'firstname', 
        'lastname',
        'phone'
    ];
    
    public static $create_rules = [
        'firstname' => 'required|string',
        'lastname' => 'required|string',
        'email' => 'required|email|unique:contacts'
    ];
    
    public function update_rules()
    {
        return [
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'email' => 'required|email|unique:contacts,email,' . $this->id
        ];
    }
    
    public function user()
    {
        return $this->belongsTo('User');
    }
    
    public function customFields()
    {
        return $this->hasMany('CustomField');
    }
}