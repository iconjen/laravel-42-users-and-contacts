<?php

class CustomField extends Eloquent {
    
    protected $fillable = [
        'contact_id',
        'value'
    ];
    
    public function contact()
    {
        return $this->belongsTo('Contact');
    }
}