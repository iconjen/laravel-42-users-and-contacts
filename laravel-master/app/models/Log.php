<?php

class Logs extends Eloquent {
    
    protected $table = 'log';
    
    protected $fillable = [
        'user_id',
        'reference_id',
        'activity'
    ];
}