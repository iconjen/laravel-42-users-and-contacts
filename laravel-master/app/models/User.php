<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    protected $fillable = [
        'email',
        'firstname', 
        'lastname',
        'password'
    ];
    
    public static $create_rules = [   
        'email'     => 'required|email|unique:users',
        'firstname' => 'required|string',
        'lastname'  => 'required|string',
        'password'  => 'required|min:8|alphadash|confirmed',
        'password_confirmation' => 'required'
    ];
    
    public function contacts()
    {
        return $this->hasMany('Contact');
    }
}
