<?php

namespace Api;

use \Validator;
use \User;
use \Input;
use \Hash;
use \Response;
use \Auth;

class UserController extends \BaseController {

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), User::$create_rules);
        
        if($validator->fails())
        {
            return Response::json([
                'status' => 'fail',
                'errors' => $validator->messages()
            ]);
        }
        
        $user = User::create(Input::all());
        
        if($user->id)
        {
            $user->password = Hash::make(Input::get('password'));
            $user->save();
            
            Auth::login($user);
            
            return Response::json([
                'status' => 'ok',
                'message' => 'New user created'
            ]);
        }else {
            return Response::json([
                'status' => 'fail',
                'errors' => ['There was a problem creating new user']
            ]);
        }
	}
}
