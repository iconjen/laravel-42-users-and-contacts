<?php

namespace Api;

class ActiveCampaignAPI {

    private $key;
    private $url;
    private $action;
    private $ch;

    public function __construct($url, $key)
    {
        $this->url = $url . '/admin/api.php';
        $this->key = $key;
        $this->ch = curl_init();
    }

    public function makeCall($action, $data = array(), $method = 'POST')
    {
        $query = '?api_action=' . $action . '&api_key=' . $this->key . '&api_output=json';;
        
        if($method == 'GET')
        {
            curl_setopt($this->ch, CURLOPT_POST, 0);
            foreach($data as $key => $value)
            {
                $query = $query . '&' . $key . '=' . $value;
            }
            
        } else {
            curl_setopt($this->ch, CURLOPT_POST, 1);
        }
        
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->ch, CURLOPT_URL, $this->url . $query);
        
        return curl_exec($this->ch);
    }
}