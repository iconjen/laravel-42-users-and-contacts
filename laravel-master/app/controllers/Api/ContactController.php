<?php

namespace Api;

use \Contact;
use \CustomField;
use \Auth;
use \View;
use \Input;
use \Validator;
use \Response;

class ContactController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $contacts = Contact::whereUserId(Auth::id())
            ->where(function($q) {
                if(Input::has('q'))
                {
                    $q->whereRaw('UPPER(email) LIKE UPPER("%' . Input::get('q') . '%")');
                }
            })
            ->paginate(15);
		
        return Response::view('contacts.index', ['contacts' => $contacts]);
	}
    
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        // Generate validator
		$validator = Validator::make(Input::all(), Contact::$create_rules);
        
        if($validator->fails())
        {
            return Response::json([
                'status' => 'fail',
                'errors' => $validator->messages()->all()
            ]);
        }
        
        // Make new contact object
        $contact = Contact::create(Input::all());
        
        return Response::json([
            'status' => 'ok',
            'message' => 'New contact added',
            'contact' => $contact
        ]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$contact = Contact::whereUserId(Auth::id())->with(['CustomFields'])->find($id);
        
        return Response::view('contacts.edit', ['contact' => $contact]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // Find the user based on ID given
        // Make sure it belongs to the current logged in user
        $contact = Contact::whereUserId(Auth::id())->find($id);
        
        // Generate validator
        $validator = Validator::make(Input::all(), $contact->update_rules());
        
        // Return fail status if validator fails
        if($validator->fails())
        {
            return Response::json([
                'status' => 'fail',
                'errors' => $validator->messages()->all()
            ]);
        }
        
        // Drop all current custom fields for contact
        CustomField::whereContactId($contact->id)->delete();
        
        // Prepare custom fields for insertion
        if(Input::has('custom_field'))
        {
            foreach(Input::get('custom_field') as $custom)
            {
                if($custom != '')
                {
                    CustomField::create(['contact_id' => $contact->id, 'value' => $custom]);
                }
            }            
        }
        
        // Update the contact
        $contact->update(Input::except(['_token', '_method']));
        $contact->save();
        
        // Return response
        return Response::json([
            'status' => 'ok',
            'message' => 'Contact updated',
            'contact' => $contact,
            'custom_fields' => $contact->CustomFields
        ]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // Find the user based on ID given
        // Make sure it belongs to the current logged in user
		$contact = Contact::whereUserId(Auth::id())->find($id);
        
        if(!count($contact))
        {
            return Response::json([
                'status' => 'fail',
                'errors' => 'Contact not found'
            ]);
        }
        
        // Delete contact from database
        $contact->delete();
        
        return Response::json([
            'status' => 'ok',
            'message' => 'Contact deleted',
            'contact' => $contact
        ]);
	}


}
