<?php
use Facebook\FacebookRequest;
use Facebook\FacebookSession;
use Facebook\GraphUser;
use MartinBean\Facebook\Laravel\FacebookRedirectLoginHelper;

class AuthController extends \BaseController {

    public function getLogin()
    {
        if(Auth::check())
        {
            return Redirect::to(url('dashboard'));
        }

        return View::make('auth.login');
    }

    public function postLogin()
    {
        if(Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')]))
        {
            return Redirect::intended(url('dashboard'));
        }

        return Redirect::back()->withError('Email and password combination incorrect')->withInput();
    }

    public function getLogout()
    {
        Auth::logout();

        return Redirect::to(url('auth/login'))->withInfo('You have successfully logged out');
    }

    public function getFacebookLogin()
    {
        FacebookSession::setDefaultApplication(
            Config::get('services.facebook.client_id'),
            Config::get('services.facebook.client_secret')
        );

        $redirectUrl = Request::url();
        $helper = new FacebookRedirectLoginHelper($redirectUrl);

        if ($session = $helper->getSessionFromRedirect())
        {
            $request = new FacebookRequest($session, 'GET', '/me?fields=id,first_name,last_name');
            $fbuser = $request->execute()->getGraphObject(GraphUser::className());
            
            $fbuser = [
                'email' => $fbuser->getId(),
                'firstname' => $fbuser->getFirstName(),
                'lastname' => $fbuser->getLastName(),
                'password' => Hash::make(' ')
            ];
            
            $user = User::whereEmail($fbuser['email'])->first();
            
            if(!count($user))
            {
                $user = User::create($fbuser);
            }
            
            Auth::login($user);
            return Redirect::intended('dashboard');
        }

        return Redirect::to($helper->getLoginUrl());
    }
}