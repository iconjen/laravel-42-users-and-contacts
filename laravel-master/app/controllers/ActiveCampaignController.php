<?php

use \Api\ActiveCampaignAPI;

class ActiveCampaignController extends \BaseController {

    private $activecampaign;

    public function __construct()
    {
        // Instantiate ActiveCampaignAPI
        $config = require app_path('config/packages/ActiveCampaign/config.php');
        $this->activecampaign = new ActiveCampaignAPI($config['url'], $config['key']);
    }

    /**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
    public function store()
    {
        $contact = Input::get();

        $ac_result = $this->activecampaign->makeCall('contact_add', [
            'email'      => $contact['email'],
            'first_name' => $contact['firstname'],
            'last_name'  => $contact['lastname'],
            'phone'      => $contact['phone'],
            'p[1]'       => '1'
        ]);

        $ac_result = json_decode($ac_result);

        if($ac_result->result_code)
        {
            Contact::whereId($contact['id'])->update(['ac_id' => $ac_result->subscriber_id]);
        } else {
            $ac_contact = $ac_result->{'0'};
            Contact::whereId($contact['id'])->update(['ac_id' => $ac_contact->id]);            
        }

        return Response::json($ac_result->result_message);
    }

    /**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function update($id)
    {
        $contact = Input::get('contact');
        
        $data = [
            'id'         => $contact['ac_id'],
            'email'      => $contact['email'],
            'first_name' => $contact['firstname'],
            'last_name'  => $contact['lastname'],
            'phone'      => $contact['phone'],
            'overwrite'  => '1',
            'p[1]'       => '1'
        ];
        
        if(Input::has('custom_fields') && count(Input::get('custom_fields')))
        {
            foreach(Input::get('contact.custom_fields') as $key => $custom_field)
            {
                $value = $custom_field['value'];
                $custom_field = explode(':', $custom_field['value']);
                
                if(count($custom_field) > 1)
                {
                    $data['field[%' . str_replace(' ', '_', strtoupper($custom_field[0])) . '%,0]'] = $custom_field[1];
                } else {
                    $key = $key === 0 ? 1 : $key;
                    $data['field[%' . urlencode('CUSTOM_FIELD_' . $key) . '%,0]'] = $value;
                }
            }
        }
        
        $ac_result = $this->activecampaign->makeCall('contact_edit', $data);
        $ac_result = json_decode($ac_result);
        
        return Response::json($ac_result->result_message);
    }

    /**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function destroy($id)
    {
        $ac_result = $this->activecampaign->makeCall('contact_delete', ['id' => $id], 'GET');
        $ac_result = json_decode($ac_result);
        return Response::json($ac_result->result_message);
    }


}
