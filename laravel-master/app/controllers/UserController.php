<?php

class UserController extends \BaseController {
    
    public function getDashboard()
    {
        $contacts = Contact::whereUserId(Auth::id())->paginate(15);
        
        return View::make('users.dashboard', ['contacts' => $contacts]);
    }
}