@extends('main')

@section('content')
<h1>Dashboard</h1>

<div class="row">
    <div class="col-sm-6">
        <a href="" data-toggle="modal" data-target="#add-new-contact-modal" class="btn btn-default" id="add-new-contact-btn"><i class="fa fa-plus"></i> New contact</a>
    </div>
    <div class="col-sm-4 pull-right">
        <div class="input-group">
            <input type="text" name="q" class="form-control" id="search-contact-field" placeholder="Search...">
            <span class="input-group-btn">
                <button class="btn btn-default" id="search-contact-btn" data-url="{{ url('api/contacts') }}">Search</button>
            </span>
        </div>
    </div>
</div>
<p></p>
<div class="modal fade" id="add-new-contact-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">Add new contact</h4>
            </div>

            {{ Form::open(['url' => url('api/contacts'), 'id' => 'add-new-contact-form', 'data-post-url' => url('activecampaign')]) }}
            <div class="modal-body">
                <div class="notifications"></div>
                <fieldset>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="firstname">First name</label>
                        <input type="text" name="firstname" id="firstname" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="lastname">Last name</label>
                        <input type="text" name="lastname" id="lastname" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="phone">Telephone</label>
                        <input type="text" name="phone" id="phone" class="form-control">
                    </div>
                </fieldset>
                <h4 class="text-center jarvis-loader hide">Processing ... <i class="fa fa-spin fa-spinner fa-xl"></i></h4>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                <button type="button" class="btn btn-default close-modal-btn" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>


<div class="modal fade" id="edit-contact-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">Edit contact</h4>
            </div>

            <div id="form-content"></div>
        </div>
    </div>
</div>

<div id="contacts-content">
    <table class="table table-bordered table-striped">
        <thead>
            <th width="50">ID</th>
            <th width="150">Email</th>
            <th width="150">Firstname</th>
            <th width="150">Lastname</th>
            <th width="150">Telephone</th>
            <th width="40">Actions</th>
        </thead>
        <tbody id="contacts">
            @if(count($contacts))
            @foreach($contacts as $contact)
            <tr>
                <td>{{ $contact->id }}</td>
                <td>{{ $contact->email }}</td>
                <td>{{ $contact->firstname }}</td>
                <td>{{ $contact->lastname }}</td>
                <td>{{ $contact->phone }}</td>
                <td>
                    <a data-target="#edit-contact-modal" data-toggle="modal" class="btn btn-primary btn-xs btn-fw edit-contact-btn" data-url="{{ url('api/contacts/' . $contact->id . '/edit') }}" title="Edit contact"><i class="fa fa-edit"></i></a>
                    <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-fw delete-contact-btn" data-url="{{ url('api/contacts/' . $contact->id) }}" title="Delete contact"><i class="fa fa-remove"></i></a>
                    <span class="jarvis-loader hide pull-right"><i class="fa fa-spin fa-spinner"></i></span>
                    <span class="clearfix"></span>
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="7">No results found</td>
            </tr>
            @endif
        </tbody>
    </table>

    {{ $contacts->links() }}
</div>
@stop