<table class="table table-bordered table-striped">
    <thead>
        <th width="50">ID</th>
        <th width="150">Email</th>
        <th width="150">Firstname</th>
        <th width="150">Lastname</th>
        <th width="150">Telephone</th>
        <th width="40">Actions</th>
    </thead>
    <tbody id="contacts">
        @if(count($contacts))
        @foreach($contacts as $contact)
        <tr>
            <td>{{ $contact->id }}</td>
            <td>{{ $contact->email }}</td>
            <td>{{ $contact->firstname }}</td>
            <td>{{ $contact->lastname }}</td>
            <td>{{ $contact->phone }}</td>
            <td>
                <a data-target="#edit-contact-modal" data-toggle="modal" class="btn btn-primary btn-xs btn-fw edit-contact-btn" data-url="{{ url('api/contacts/' . $contact->id . '/edit') }}" title="Edit contact"><i class="fa fa-edit"></i></a>
                <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-fw delete-contact-btn" data-url="{{ url('api/contacts/' . $contact->id) }}" title="Delete contact"><i class="fa fa-remove"></i></a>
                <span class="jarvis-loader hide"><i class="fa fa-spin fa-spinner"></i></span>
            </td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="7">No results found</td>
        </tr>
        @endif
    </tbody>
</table>

{{ $contacts->links() }}