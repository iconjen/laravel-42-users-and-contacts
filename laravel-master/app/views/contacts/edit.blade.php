{{ Form::model($contact, ['url' => url('api/contacts/' . $contact->id), 'id' => 'edit-contact-form']) }}
<div class="modal-body">
    <div class="notifications"></div>
    <fieldset>
        <div class="form-group">
            <label for="edit-email">Email</label>
            {{ Form::email('email', null, ['id' => 'edit-email', 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            <label for="edit-firstname">First name</label>
            {{ Form::text('firstname', null, ['id' => 'edit-firstname', 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            <label for="edit-lastname">Last name</label>
            {{ Form::text('lastname', null, ['id' => 'edit-lastname', 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            <label for="edit-phone">Telephone</label>
            {{ Form::text('phone', null, ['id' => 'edit-phone', 'class' => 'form-control']) }}
        </div>

        <div id="custom-fields">
            @foreach($contact->CustomFields as $key => $custom_field)
            <div class="form-group" id="custom-field-group-{{ $key }}">
                <label for="custom-field-{{ $custom_field->id }}">Custom field</label>
                <div class="input-group">
                    {{ Form::text('custom_field[]', $custom_field->value, ['id' => 'custom-field-' . $custom_field->id, 'class' => 'form-control custom-field']) }}
                    <span class="input-group-btn">
                        <a href="" class="remove-custom-field-btn btn btn-danger" data-id="#custom-field-group-{{ $key }}"><i class="fa fa-remove"></i></a>
                    </span>
                </div>
            </div>
            @endforeach            
        </div>
        
        <a href="" id="add-custom-field-btn" class="btn btn-default" style="<?php if(count($contact->CustomFields) == 5) { echo 'display:none'; } ?>"><i class="fa fa-plus fa-fw"></i></a>
        
    </fieldset>
    <h4 class="text-center jarvis-loader hide">Processing ... <i class="fa fa-spin fa-spinner fa-xl"></i></h4>
</div>
<div class="modal-footer">
    <input type="hidden" name="_method" value="PUT">
    <button type="button" class="btn btn-default close-modal-btn" data-dismiss="modal">Close</button>
    <input type="submit" class="btn btn-primary" value="Submit">
</div>
{{ Form::close() }}