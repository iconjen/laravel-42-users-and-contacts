<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Marketing Genesis Philippines</title>

        {{ HTML::script(asset('node_modules/jquery/dist/jquery.min.js')) }}
        {{ HTML::script(asset('node_modules/bootstrap/dist/js/bootstrap.min.js')) }}
        {{ HTML::style(asset('node_modules/bootstrap/dist/css/bootstrap.min.css')) }}
        {{ HTML::style(asset('node_modules/bootstrap/dist/css/bootstrap-theme.min.css')) }}

        {{ HTML::style(asset('assets/css/style.css')) }}
        {{ HTML::script(asset('assets/js/script.js')) }}
        {{ HTML::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css') }}
    </head>

    <body data-page="{{ Input::get('page') or 1 }}">

        <div class="container" role="main">
            <nav class="navbar navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="{{ url() }}">Home</a></li>
                                @if(Auth::check())
                                <li><a href="{{ url('auth/logout') }}">Logout</a></li>
                                @endif
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </div>
            </nav>
            <div id="main">
                @yield('content')
            </div>
        </div>
    </body>
</html>