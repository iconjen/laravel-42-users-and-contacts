@extends('main')

@section('content')
<h1>User login</h1>

<div class="modal fade" id="user-registration-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">Register new account</h4>
            </div>

            <div class="modal-body hide" id="form-notifications">
                <div class="alert" id="notification"></div>
                <h2 id="jarvis-spinner" class="text-center"><i class="fa fa-spin fa-spinner"></i></h2>
            </div>
            {{ Form::open(['url' => url('api/users'), 'id' => 'user-signup-form', 'data-redirect-url' => url('dashboard')]) }}
            <div class="modal-body">
                <div class="notifications"></div>
                <fieldset>
                    <div class="form-group">
                        <label for="email-register">Email</label>
                        <input type="email" id="email-register" class="form-control" name="email">
                    </div>
                    <div class="form-group">
                        <label for="firstname-register">First name</label>
                        <input type="text" id="firstname-register" class="form-control" name="firstname">
                    </div>
                    <div class="form-group">
                        <label for="lastname-register">Last name</label>
                        <input type="text" id="lastname-register" class="form-control" name="lastname">
                    </div>
                    <div class="form-group">
                        <label for="password-register">Password</label>
                        <input type="password" id="password-register" class="form-control" name="password">
                    </div>
                    <div class="form-group">
                        <label for="confirm-password-register">Confirm password</label>
                        <input type="password" id="confirm-password-register" class="form-control" name="password_confirmation">
                    </div>
                    <hr>

                    <p class="text-center">
                        <a href="{{ url('auth/facebook-login') }}"><img src="{{ asset('assets/img/fb-login.png') }}" width="250px"></a>
                    </p>
                </fieldset>
                <h4 class="text-center jarvis-loader hide">Processing ... <i class="fa fa-spin fa-spinner fa-xl"></i></h4>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default close-modal-btn" data-dismiss="modal">Close</button>
                <input type="submit" value="Sign up" class="btn btn-primary">
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4 col-xs-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">User Login</h3>
            </div>
            {{ Form::open(['url' => url('auth/login')]) }}
            <div class="panel-body">
                @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('error') }}
                </div>
                @endif
                @if(Session::has('info'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('info') }}
                </div>
                @endif
                @if(count($errors))
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
                <div class="form-group">
                    <label for="username">Email</label>
                    <input type="text" name="email" value="" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control">
                </div>
                <hr>
                <p class="text-center">
                    <a href="{{ url('auth/facebook-login') }}"><img src="{{ asset('assets/img/fb-login.png') }}" width="250px"></a>
                </p>
            </div>
            <div class="panel-footer">
                <input type="submit" value="Log in" class="btn btn-primary">
                <p class="pull-right">New user? Register <a data-target="#user-registration-modal" data-toggle="modal" href="">HERE</a>.</p>
                <div class="clearfix"></div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop