<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Vodien Technical Support Leaderboard</title>
	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);

		body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:center;
			color: #999;
		}

		.welcome {
			width: 300px;
			height: 200px;
			position: absolute;
			left: 50%;
			top: 50%;
			margin-left: -150px;
			margin-top: -100px;
		}

		a, a:visited {
			text-decoration:none;
		}

		h1 {
			font-size: 32px;
			margin: 16px 0 0 0;
		}
	</style>
</head>
<body>
    <h1>Leaderboard</h1>
    <iframe frameborder="0" src="https://analytics.zendesk.com/reportWidget.html#project=/gdc/projects/tm4quycqn8eaow14g52edx5izj2byc6j&report=/gdc/md/tm4quycqn8eaow14g52edx5izj2byc6j/obj/64759&title=yes" width="280px" height="400px" allowTransparency="false"></iframe>
    <iframe frameborder="0" src="https://analytics.zendesk.com/reportWidget.html#project=/gdc/projects/tm4quycqn8eaow14g52edx5izj2byc6j&report=/gdc/md/tm4quycqn8eaow14g52edx5izj2byc6j/obj/64779&title=yes" width="280px" height="400px" allowTransparency="false"></iframe>
</body>
</html>
