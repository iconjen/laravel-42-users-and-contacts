This is a simple phonebook project that runs on Laravel 4.2

#Deployment instructions
1. Clone repository
2. Navigate to laravel-master and run composer install
3. Create database
4. Configure /laravel-master/app/config/database.php
5. Navigate to laravle-master and run php artisan migrate --force
6. Run php artisan serve OR configure virtual host to point to public_html
7. Configure /laravel-master/app/config/services.php for Facebook client key

#Notes
1. The Facebook sign in uses the user's ID instead of email when signing up