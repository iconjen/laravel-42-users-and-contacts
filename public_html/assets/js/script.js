$(document).ready(function() {
    
    var refreshPage = function() {
        $.get('api/contacts', {}, function(response) {
            $('#contacts-content').html(response);
        });
    };
    
    var userSignup = function() {
        $('#user-signup-form').on('submit', function(e) {
            e.preventDefault();
            
            var form = $(this),
                data = {};
            
            form.children().find('.notifications').html('');
            form.children().find('fieldset').hide();
            form.children().find('.jarvis-loader').removeClass('hide').show();
            
            $.each(form.serializeArray(), function(i, j) {
                data[j.name] = j.value;
            });
            
            $.post(form.attr('action'), data, function(response) {
                if(response.status == 'ok') {
                    form.children().find('.notifications').html('<div class="alert alert-success">' + response.message + '</div>');
                    form.children().find('input[type="submit"]').remove();
                    window.setTimeout(function() {
                        window.location = form.data('redirect-url');
                    }, 2000);
                } else {
                    var messages = '';
                    $.each(response.errors, function(i, j) {
                        messages = messages + '<p>' + j + '</p>';
                    });
                    
                    form.children().find('fieldset').show();
                    form.children().find('.notifications').html('<div class="alert alert-danger">' + messages + '</div>');
                }
            }).done(function() {
                form.children().find('.jarvis-loader').hide();
            });
        });
    };
    
    userSignup();
    
    $('#add-new-contact-btn').on('click', function() {
        $('#add-new-contact-form')[0].reset();
        $('#add-new-contact-form').children().find('.notifications').html('')
    });
    
    var createContact = function() { 
        $('#add-new-contact-form').on('submit', function(e) {
            e.preventDefault();
            
            var form = $(this),
                data = {};
            
            form.children().find('.notifications').html('');
            form.children().find('fieldset').hide();
            form.children().find('.jarvis-loader').removeClass('hide').show();
            
            $.each(form.serializeArray(), function(i, j) {
                data[j.name] = j.value;
            });
            
            $.post(form.attr('action'), data, function(response) {
                if(response.status == 'ok') {
                    form.children().find('.notifications').html('<div class="alert alert-success">' + response.message + '</div>');
                    $('.close-modal-btn').click();
                    refreshPage();
                } else {
                    var messages = '';
                    $.each(response.errors, function(i, j) {
                        messages = messages + '<p>' + j + '</p>';
                    });
                    
                    form.children().find('.notifications').html('<div class="alert alert-danger">' + messages + '</div>');
                }
            }).done(function(response) {
                form.children().find('fieldset').show();
                form.children().find('.jarvis-loader').hide();
                
                if(response.status == 'ok') {
                    $.post($('#add-new-contact-form').data('post-url'), response.contact, function(response) {
                        console.log(response);
                    });
                }
            });
        });
    };
    
    createContact();
    
    $(document).on('click', '.edit-contact-btn', function() {
        var form_content = $('#edit-contact-modal').children().find('#form-content');

        form_content.html('<h4 class="text-center jarvis-loader">Loading ... <i class="fa fa-spin fa-spinner fa-xl"></i></h4>')
        $.get($(this).data('url'), {}, function(response) {
            form_content.html(response);
        });
    });
    
    var editContact = function() { 
        $(document).on('submit', '#edit-contact-form', function(e) {
            e.preventDefault();
            
            var form = $(this),
                data = {};
            
            form.children().find('.notifications').html('');
            form.children().find('fieldset').hide();
            form.children().find('.jarvis-loader').removeClass('hide').show();
            
            $.each(form.serializeArray(), function(i, j) {
                
                if(j.name.indexOf('[]') > 0) {
                    if(data[j.name] === undefined) {
                        data[j.name] = [];
                    }
                    data[j.name].push(j.value);
                } else {
                    data[j.name] = j.value;
                }
            });
            
            $.post(form.attr('action'), data, function(response) {
                if(response.status == 'ok') {
                    form.children().find('.notifications').html('<div class="alert alert-success">' + response.message + '</div>');
                    $('.close-modal-btn').click();
                    refreshPage();
                } else {
                    var messages = '';
                    $.each(response.errors, function(i, j) {
                        messages = messages + '<p>' + j + '</p>';
                    });
                    
                    form.children().find('.notifications').html('<div class="alert alert-danger">' + messages + '</div>');
                }
            }).done(function(response) {
                form.children().find('fieldset').show();
                form.children().find('.jarvis-loader').hide();
                
                if(response.status == 'ok') {
                    var data = {
                        _method: 'PUT',
                        contact: response.contact,
                        custom_fields: response.custom_fields
                    }
                    
                    $.post('activecampaign/' + response.contact.ac_id, data, function(response) {
                        console.log(response);
                    });
                }
            });
        });
    };
    
    editContact();
    
    var deleteContact = function() {
        $(document).on('click', '.delete-contact-btn', function() {
            var url = $(this).data('url');
            $(this).parent().parent().children().find('.jarvis-loader').removeClass('hide').show();
            
            $.post(url, {_method: 'DELETE'}, function(response) {
                if(response.status == 'ok') {
                    refreshPage();
                } else {
                    alert(response.errors);
                }
            }).done(function(response){
                if(response.status == 'ok') {
                    
                    $.post('activecampaign/' + response.contact.ac_id, {_method:'DELETE'}, function(response) {
                        console.log(response);
                    });
                }
            });
        });
    };
    
    deleteContact();
    
    var addCustomField = function() {
        $(document).on('click', '#add-custom-field-btn', function(e) {
            e.preventDefault();
            
            var fieldCount = $('.custom-field').length,
                i = fieldCount + 1,
                label = '<label for="custom-field-' + i + '">Custom field</label>',
                input = '<input name="custom_field[]" type="text" id="custom-field-' + i +'" class="form-control custom-field" >',
                addon = '<span class="input-group-btn"><a href="" class="remove-custom-field-btn btn btn-danger" data-id="#custom-field-group-' + i + '"><i class="fa fa-remove"></i></a></span>',
                customField = '<div class="form-group" id="custom-field-group-' + i + '">' + label + '<div class="input-group">' + input + addon + '</div></div>';
            
            if(fieldCount <= 5) {
                $('#custom-fields').append(customField);
            }
            
            if((fieldCount + 1) == 5) {
                $('#add-custom-field-btn').hide();
            }
        });
    };
    
    addCustomField();
    
    var removeCustomField = function() {
        $(document).on('click', '.remove-custom-field-btn', function(e) {
            e.preventDefault();
            
            var id = $(this).data('id');
            $(id).remove();
            
            var fieldCount = $('.custom-field').length;
            
            if(fieldCount == 5 ) {
                $('#add-custom-field-btn').hide();
            } else {
                $('#add-custom-field-btn').show();
            }
        });
    };
    
    removeCustomField();
    
    var searchContact = function() {
        $('#search-contact-btn').on('click', function(e) {
            var data = {
                q: $('#search-contact-field').val()
            };
            
            $.get($(this).data('url'), data, function(response) {
                $('#contacts-content').html(response);
            });
        });
    };
    
    searchContact();
});